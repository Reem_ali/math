package com.example.reemh.math.number;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.reemh.math.R;

public class NumbersTextToSpeechActivity extends AppCompatActivity  implements TextToSpeech.OnInitListener{

    TextToSpeech textToSpeech;
    Button exersice_1;

    Button exersice_2;
    Button exersice_3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_grade_exercises);
        exersice_1 = (Button)findViewById(R.id.button_k1);
        exersice_2=(Button)findViewById(R.id.button_k2);
        exersice_3 =(Button)findViewById(R.id.butten_k3);
        textToSpeech =new TextToSpeech(this,this);
    }


    public void numberSequence (View v){
        Intent intent = new Intent(NumbersTextToSpeechActivity.this,NumberSequenceRepresentingActivity.class);
        textToSpeech.speak(exersice_1.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);

        startActivity(intent);
    }
    public void open_exercise(View view){
        Intent intent_1 = new Intent(NumbersTextToSpeechActivity.this,NumberSequenceRepresentingActivity.class);
        startActivity(intent_1);
    }




    public void counting_Numbers(View view){

        textToSpeech.speak(exersice_3.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);


    }
    @Override
    public void onInit(int status) {

    }
}
