package com.example.reemh.math.exercises;

import android.content.ClipData;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.example.reemh.math.R;
import com.example.reemh.math.speech.TextToSpeechOnInitListener;
import com.example.reemh.math.utils.NumbersUtils;

import java.util.ArrayList;
import java.util.List;

public class DragAndDropSummationActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {



    private Drawable whiteContainerEnterMode;
    private Drawable whiteContainerChangeMode;
    private Drawable orangeContainer;
    private Drawable greenContainer;
    private Drawable orangeContainerDropped;
    private Drawable greenContainerDropped;
    private Drawable whiteColor;

    private TextToSpeechOnInitListener textToSpeach;

    private final List<View> droppedItems = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag_and_drop_summation);
        initDrawables();

        int num1 = NumbersUtils.getRandomAddend(NumbersUtils.MAX_NUM);
        int num2 = NumbersUtils.getRandomAddend(NumbersUtils.MAX_NUM - num1);

        LinearLayout resultLayout = (LinearLayout) findViewById(R.id.result_container);
        resultLayout.setOnDragListener(new AddendLayoutDragListener(whiteContainerEnterMode, whiteContainerChangeMode, resultLayout));

        LinearLayout leftAddendLayout = (LinearLayout) findViewById(R.id.left_addend_container);
        leftAddendLayout.setOnDragListener(new AddendLayoutDragListener(whiteContainerEnterMode, whiteContainerChangeMode, resultLayout));
        prepareAddendLayouts(leftAddendLayout, num1, orangeContainer);

        LinearLayout rightAddendLayout = (LinearLayout) findViewById(R.id.right_addend_container);
        rightAddendLayout.setOnDragListener(new AddendLayoutDragListener(whiteContainerEnterMode, whiteContainerChangeMode, resultLayout));
        prepareAddendLayouts(rightAddendLayout, num2, greenContainer);

        LinearLayout plusLayout = (LinearLayout) findViewById(R.id.plus_container);
        plusLayout.setOnDragListener(new AddendLayoutDragListener(null, null, resultLayout));

        LinearLayout equalsLayout = (LinearLayout) findViewById(R.id.equals_container);
        equalsLayout.setOnDragListener(new AddendLayoutDragListener(null, null, resultLayout));
    }

    private void prepareAddendLayouts(LinearLayout linearLayout, int num, Drawable drawable) {
        for (int i = 0; i < num; i++) {
            LinearLayout viewLayout = createContainerLayout(drawable);
            viewLayout.setOnLongClickListener(new AddendTouchListener());
            linearLayout.addView(viewLayout, i);
        }
    }

    private LinearLayout createWhiteSpaceLayout() {
        LinearLayout linearLayout = createNewBasicLayout();
        linearLayout.setBackground(whiteColor);
        return linearLayout;
    }

    private LinearLayout createContainerLayout(Drawable drawable) {
        LinearLayout linearLayout = createNewBasicLayout();
        linearLayout.setBackground(drawable);
        return linearLayout;
    }

    private LinearLayout createNewBasicLayout() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1);
        layoutParams.setMargins(2, 2, 2, 0);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        return linearLayout;
    }

    public void initDrawables() {
        whiteContainerEnterMode = ContextCompat.getDrawable(this, R.drawable.shape_white_container_blue_strokes);
        whiteContainerChangeMode = ContextCompat.getDrawable(this, R.drawable.shape_white_container_red_strokes);
        orangeContainer = ContextCompat.getDrawable(this, R.drawable.shape_orange_container);
        greenContainer = ContextCompat.getDrawable(this, R.drawable.shape_green_container);
        orangeContainerDropped = ContextCompat.getDrawable(this, R.drawable.shape_orange_dropped_conatiner);
        greenContainerDropped = ContextCompat.getDrawable(this, R.drawable.shape_green_dropped_container);
        whiteColor = ContextCompat.getDrawable(this, R.color.white);

        textToSpeach = new TextToSpeechOnInitListener(this);
    }

    @Override
    public void onInit(int i) {

    }

    private final class AddendTouchListener implements View.OnLongClickListener {

        @Override
        public boolean onLongClick(View view) {
            if (!droppedItems.contains(view)) {
                droppedItems.add(view);

                ClipData data = ClipData.newPlainText("", "");

                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                view.startDrag(data, shadowBuilder, createContainerLayout(view.getBackground()), 0);

                if (view.getBackground() == greenContainer) {
                    view.setBackground(greenContainerDropped);
                } else {
                    view.setBackground(orangeContainerDropped);
                }

                textToSpeach.speakOut(String.valueOf(droppedItems.size()));
                return true;
            }
            return false;
        }

    }

}
