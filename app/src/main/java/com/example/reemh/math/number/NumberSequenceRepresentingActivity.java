package com.example.reemh.math.number;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.reemh.math.R;
import com.example.reemh.math.speech.TextToSpeechOnInitListener;

public class NumberSequenceRepresentingActivity extends AppCompatActivity {

    private final static int[] BUTTON_IDS = {
            R.id.b1, R.id.b2, R.id.b3, R.id.b4,
            R.id.b5, R.id.b6, R.id.b7, R.id.b8,
            R.id.b9, R.id.b10, R.id.b11, R.id.b12,
            R.id.b13, R.id.b14, R.id.b15, R.id.b16,
            R.id.b17, R.id.b18, R.id.b19,
            R.id.b20};

    private Button nextExercise;
    private  Button backExersice;
    private TextToSpeechOnInitListener speech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers_sequence_exercise_1);

        speech = new TextToSpeechOnInitListener(this);
        nextExercise = (Button) findViewById(R.id.next_exercise);
        backExersice =(Button)findViewById(R.id.back_exercise);

        for (int id : BUTTON_IDS) {
            final Button button = (Button) findViewById(id);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    speech.speakOut(button.getText().toString());
                }
            });
        }
    }
    public void nextExercise(View v) {
        Intent intent = new Intent(NumberSequenceRepresentingActivity.this,NumberSequenceFirstExerciseActivity.class);
        speech.speakOut(nextExercise.getText().toString());
        startActivity(intent);
    }
    public void backEersice(View v) {
        Intent intent = new Intent(NumberSequenceRepresentingActivity.this,NumbersTextToSpeechActivity.class);
        speech.speakOut(backExersice.getText().toString());
        startActivity(intent);
    }






}


