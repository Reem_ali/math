package com.example.reemh.math.grade;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.reemh.math.exercises.DragAndDropSummationActivity;
import com.example.reemh.math.number.NumbersTextToSpeechActivity;
import com.example.reemh.math.R;
import com.example.reemh.math.number.NumbersFirstGradeActivity;


public class FirstGradeActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {


    private TextToSpeech textToSpeech;
    private Button numbersButton;
    private Button numberExerciseButton;
    private Button additionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_grade);

        numbersButton =(Button)findViewById(R.id.button_k1_numbers);
        numberExerciseButton =(Button)findViewById(R.id.button_k1_numbers_ex);
        additionButton =(Button)findViewById(R.id.butten_k1_Add);

        textToSpeech = new TextToSpeech(this,this);
    }
    public void openNumbersActivity(View v){
        Intent intent = new Intent(FirstGradeActivity.this,NumbersFirstGradeActivity.class);
        textToSpeech.speak(numbersButton.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
        startActivity(intent);
    }

    public void openNumbersExerciseActivity(View view){
        Intent intent = new Intent(FirstGradeActivity.this,NumbersTextToSpeechActivity.class);
        textToSpeech.speak(numberExerciseButton.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
        startActivity(intent);
    }

/**

    public void openAdditionActivity(View view){
        Intent intent = new Intent(FirstGradeActivity.this,NumbersTextToSpeechActivity.class);
        textToSpeech.speak(additionButton.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
        startActivity(intent);
    }

**/


    public void openAdditionActivity(View view){
        Intent intent = new Intent(FirstGradeActivity.this,DragAndDropSummationActivity.class);
        textToSpeech.speak(additionButton.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
        startActivity(intent);
    }

    @Override
    public void onInit(int status) {

    }
}
