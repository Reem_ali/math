package com.example.reemh.math.number;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.reemh.math.R;
import com.example.reemh.math.speech.TextToSpeechOnInitListener;

public class NumberSequenceFirstExerciseActivity extends AppCompatActivity {
    EditText editText_1;
    EditText editText_2;
    EditText editText_3;
    EditText editText_4;
    private Button nextExercise;
    private  Button backExersice;
    private TextToSpeechOnInitListener speech;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers_sequence_exercise_2);
        editText_2 = (EditText) findViewById(R.id.e2);
        editText_3 = (EditText) findViewById(R.id.e3);
        editText_4 = (EditText) findViewById(R.id.e4);
        speech = new TextToSpeechOnInitListener(this);
        nextExercise = (Button) findViewById(R.id.next_exercise);
        backExersice =(Button)findViewById(R.id.back_exercise);



        editText_1 = (EditText) findViewById(R.id.e1);
        editText_1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                //
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
                //
            }


            @Override
            public void afterTextChanged(Editable arg0) {
                String value = editText_1.getText().toString();
                int finalValue = -1;
                if (value != null && value.length() > 0) {
                    try {
                        finalValue = Integer.valueOf(value);
                    } catch (NumberFormatException e) {

                    }
                }

                if (finalValue == 2) {
                    Context context_1 = getApplicationContext();
                    CharSequence text = "Prima";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast_1 = Toast.makeText(context_1, text, duration);
                    toast_1.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);

                    toast_1.show();
                } else {


                    Context context_2 = getApplicationContext();
                    CharSequence text = "Versuch nochmal";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast_2 = Toast.makeText(context_2, text, duration);
                    toast_2.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
                    toast_2.show();

                }
            }

        });
        editText_2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                //
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
                //
            }


            @Override
            public void afterTextChanged(Editable arg0) {
                String value = editText_2.getText().toString();
                int finalValue = -1;
                if (value != null && value.length() > 0) {
                    try {
                        finalValue = Integer.valueOf(value);
                    } catch (NumberFormatException e) {

                    }
                }

                if (finalValue == 6) {
                    Context context_1 = getApplicationContext();
                    CharSequence text = "Prima";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast_1 = Toast.makeText(context_1, text, duration);
                    toast_1.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);

                    toast_1.show();
                } else {


                    Context context_2 = getApplicationContext();
                    CharSequence text = "Versuch nochmal";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast_2 = Toast.makeText(context_2, text, duration);
                    toast_2.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
                    toast_2.show();

                }
            }

        });
        editText_3.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                //
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
                //
            }


            @Override
            public void afterTextChanged(Editable arg0) {
                String value = editText_3.getText().toString();
                int finalValue = -1;
                if (value != null && value.length() > 0) {
                    try {
                        finalValue = Integer.valueOf(value);
                    } catch (NumberFormatException e) {

                    }
                }

                if (finalValue == 13) {
                    Context context_1 = getApplicationContext();
                    CharSequence text = "Prima";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast_1 = Toast.makeText(context_1, text, duration);
                    toast_1.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);

                    toast_1.show();
                } else {


                    Context context_2 = getApplicationContext();
                    CharSequence text = "Versuch nochmal";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast_2 = Toast.makeText(context_2, text, duration);
                    toast_2.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
                    toast_2.show();

                }
            }

        });
        editText_4.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                //
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
                //
            }


            @Override
            public void afterTextChanged(Editable arg0) {
                String value = editText_4.getText().toString();
                int finalValue = -1;
                if (value != null && value.length() > 0) {
                    try {
                        finalValue = Integer.valueOf(value);
                    } catch (NumberFormatException e) {

                    }
                }

                if (finalValue == 19) {
                    Context context_1 = getApplicationContext();
                    CharSequence text = "Prima";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast_1 = Toast.makeText(context_1, text, duration);
                    toast_1.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);

                    toast_1.show();
                } else {


                    Context context_2 = getApplicationContext();
                    CharSequence text = "Versuch nochmal";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast_2 = Toast.makeText(context_2, text, duration);
                    toast_2.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
                    toast_2.show();

                }
            }

        });
    }

        public void nextExercise(View v) {
        Intent intent = new Intent(NumberSequenceFirstExerciseActivity.this, NumberSeqThirdExersice.class);
    ;
        speech.speakOut(nextExercise.getText().toString());
        startActivity(intent);
    }
       public void backExersice(View view){
           Intent intent= new Intent(NumberSequenceFirstExerciseActivity.this,NumberSequenceRepresentingActivity.class);
           speech.speakOut(backExersice.getText().toString());
           startActivity(intent);
       }


    /**
     * Created by reemh on 21.07.2017.
     */

}

