package com.example.reemh.math.exercises;

import android.graphics.drawable.Drawable;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.reemh.math.R;
import com.example.reemh.math.speech.TextToSpeechOnInitListener;

import java.util.ArrayList;
import java.util.List;

public class AddendLayoutDragListener implements View.OnDragListener {

    private Drawable enterShape;
    private Drawable normalShape;
    private ViewGroup target;

    public AddendLayoutDragListener(Drawable normalShape, Drawable enterShape, ViewGroup target) {
        this.enterShape = enterShape;
        this.normalShape = normalShape;
        this.target = target;
    }

    @Override
    public boolean onDrag(View view, DragEvent event) {
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED: //signal for the start of a drag and drop operation.
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                view.setBackground(enterShape);
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                view.setBackground(normalShape); //change the shape of the view back to normal
                break;
            case DragEvent.ACTION_DROP:
                drop(view, event);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                view.setBackground(normalShape);
            default:
                break;
        }
        return true;
    }

    private void drop(View view, DragEvent event) {
        View droppedView = (View) event.getLocalState();
        droppedView.setVisibility(View.VISIBLE);
        if (view.getId() == R.id.result_container) {
            LinearLayout container = (LinearLayout) view;
            container.addView(droppedView);
        } else{
            target.addView(droppedView);
        }
    }
}
