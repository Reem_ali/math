package com.example.reemh.math;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.reemh.math.grade.FirstGradeActivity;
import com.example.reemh.math.grade.SecondGradeActivity;

public class MainActivity extends AppCompatActivity  implements TextToSpeech.OnInitListener{

    private TextToSpeech textToSpeech;

    private Button firstGradeButton;

    private Button secondGradeButton;

    private Button resultsButton;

    private Button appExitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textToSpeech = new TextToSpeech(this,this);

        firstGradeButton =(Button)findViewById(R.id.button_k1);
        secondGradeButton =(Button)findViewById(R.id.button_k2);
        resultsButton =(Button)findViewById(R.id.button_er);
        appExitButton = (Button)findViewById(R.id.button_Exist);
    }

     public void openFirstGrade(View v){
         Intent intent = new Intent(MainActivity.this,FirstGradeActivity.class);
         textToSpeech.speak(firstGradeButton.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
         startActivity(intent);
     }


     public void openSecondGrade(View view){
         Intent intent2 =new Intent(MainActivity.this,SecondGradeActivity.class);
         textToSpeech.speak(secondGradeButton.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
         startActivity(intent2);

     }

    public void openResults(View view){
        textToSpeech.speak(resultsButton.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
    }

    public void quitApplication(View view){
        textToSpeech.speak(appExitButton.getText().toString(),TextToSpeech.QUEUE_FLUSH,null);
    }

    @Override
    public void onInit(int status) {

    }
}
