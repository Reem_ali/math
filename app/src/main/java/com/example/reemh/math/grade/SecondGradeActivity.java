package com.example.reemh.math.grade;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.reemh.math.R;

public class SecondGradeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_grade);
    }
}
