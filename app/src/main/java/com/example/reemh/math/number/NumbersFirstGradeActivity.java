package com.example.reemh.math.number;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.reemh.math.R;
import com.example.reemh.math.model.NumberModel;

import java.util.ArrayList;

public class NumbersFirstGradeActivity extends AppCompatActivity {

    private final static int[] IMAGES_IDS = {R.drawable.image_number_with_text_0, R.drawable.image_number_with_text_1, R.drawable.image_number_with_text_2, R.drawable.image_number_with_text_3, R.drawable.image_number_with_text_4, R.drawable.image_number_with_text_5, R.drawable.image_number_with_text_6, R.drawable.image_number_with_text_7, R.drawable.image_number_with_text_8,
            R.drawable.image_number_with_text_9, R.drawable.image_number_with_text_10, R.drawable.image_number_with_text_11, R.drawable.image_number_with_text_12, R.drawable.image_number_with_text_13, R.drawable.image_number_with_text_14, R.drawable.image_number_with_text_15, R.drawable.image_number_with_text_16, R.drawable.image_number_with_text_17, R.drawable.image_number_with_text_18,
            R.drawable.image_number_with_text_19, R.drawable.image_number_with_text_20,R.drawable.h1,
            R.drawable.h2,R.drawable.h3,R.drawable.h4,R.drawable.h5,R.drawable.h6,R.drawable.h7,R.drawable.h8,
            R.drawable.h9,R.drawable.h10,R.drawable.h11,R.drawable.h12,R.drawable.h13,R.drawable.h14,R.drawable.h15,
            R.drawable.h16,R.drawable.h17,R.drawable.h18,R.drawable.h19,R.drawable.h20};

    private ViewPager viewPager;
    private ArrayList<NumberModel> numberModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_numbers_with_square_images);

        viewPager = (ViewPager) findViewById(R.id.view_pager);

        for (int i = 0; i < IMAGES_IDS.length; i++) {
            numberModelList.add(new NumberModel("Zahl:" + i, IMAGES_IDS[i]));
        }

        SwipPage swip = new SwipPage(this, numberModelList);

        viewPager.setAdapter(swip);
    }

    private class SwipPage extends PagerAdapter {

        private LayoutInflater layoutInflater;
        private Context context;
        private ArrayList<NumberModel> numberModel = new ArrayList<>();


        public SwipPage(Context context, ArrayList<NumberModel> numberModel) {

            this.context = context;
            this.numberModel = numberModel;
        }

        @Override
        public int getCount() {
            return numberModel.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == ((LinearLayout) object));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.item_number_slide, container, false);

            ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
            imageView.setImageResource(numberModel.get(position).getId());

            TextView textView = (TextView) view.findViewById(R.id.textView);
            textView.setText(numberModel.get(position).getName());

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }


}
