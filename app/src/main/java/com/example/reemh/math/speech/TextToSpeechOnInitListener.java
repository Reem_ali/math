package com.example.reemh.math.speech;

import android.app.Activity;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

public class TextToSpeechOnInitListener implements TextToSpeech.OnInitListener {

    private TextToSpeech textToSpeech;

    public TextToSpeechOnInitListener(Activity activity) {
        textToSpeech = new TextToSpeech(activity, this);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(Locale.GERMANY);
        }
    }

    public void speakOut(String text) {
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);

    }
}
